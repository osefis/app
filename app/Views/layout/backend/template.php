<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('plugins/fontawesome-free/css/all.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'); ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('plugins/icheck-bootstrap/icheck-bootstrap.min.css'); ?>">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url('plugins/jqvmap/jqvmap.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('dist/css/adminlte.min.css'); ?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url('plugins/overlayScrollbars/css/OverlayScrollbars.min.css'); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('plugins/daterangepicker/daterangepicker.css'); ?>">
  <!-- summernote -->
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-bs4.min.css" rel="stylesheet">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<?= base_url(); ?>/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?= base_url(); ?>/plugins/toastr/toastr.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- TABLE -->
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('plugins/datatables-responsive/css/responsive.bootstrap4.min.css'); ?>">
  <!-- END TABEL -->

  <!-- My CSS -->
  <link rel="stylesheet" href="<?= base_url('/css/style.css'); ?>">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <?= $this->include('layout/backend/navbar'); ?>
    <?= $this->include('layout/backend/sidebar'); ?>



    <?= $this->renderSection('content'); ?>

  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    Footer
  </div>
  <!-- /.card-footer-->
  </div>
  <!-- /.card -->

  </section>
  <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?= $this->include('layout/backend/footer'); ?>

  <!-- jQuery -->
  <script src="<?= base_url('plugins/jquery/jquery.min.js'); ?>"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= base_url('plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url('plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <!-- ChartJS -->
  <script src="<?= base_url('plugins/chart.js/Chart.min.js'); ?>"></script>
  <!-- Sparkline -->
  <script src="<?= base_url('plugins/sparklines/sparkline.js'); ?>"></script>
  <!-- JQVMap -->
  <script src="<?= base_url('plugins/jqvmap/jquery.vmap.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/jqvmap/maps/jquery.vmap.usa.js'); ?>"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?= base_url('plugins/jquery-knob/jquery.knob.min.js'); ?>"></script>
  <!-- daterangepicker -->
  <script src="<?= base_url('plugins/moment/moment.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/daterangepicker/daterangepicker.js'); ?>"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?= base_url('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'); ?>"></script>
  <!-- Summernote -->
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="<?= base_url('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'); ?>"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url('dist/js/adminlte.js'); ?>"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?= base_url('dist/js/pages/dashboard.js'); ?>"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url('dist/js/demo.js'); ?>"></script>

  <!-- TABEL -->
  <!-- DataTables -->
  <script src="<?= base_url('plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/datatables-responsive/js/dataTables.responsive.min.js'); ?>"></script>
  <script src="<?= base_url('plugins/datatables-responsive/js/responsive.bootstrap4.min.js'); ?>"></script>

  <!-- SweetAlert2 -->
  <script src="<?= base_url(); ?>/plugins/sweetalert2/sweetalert2.min.js"></script>
  <!-- Toastr -->
  <script src="<?= base_url(); ?>/plugins/toastr/toastr.min.js"></script>

  <script>
    $(function() {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
  <!-- END TABLE -->

  <script>
    function previewImg() {
      const image = document.querySelector('#image');
      const imageLabel = document.querySelector('.custom-file-label');
      const imgPreview = document.querySelector('.img-preview');

      imageLabel.textContent = image.files[0].name;

      const fileImage = new FileReader();
      fileImage.readAsDataURL(image.files[0]);

      fileImage.onload = function(e) {
        imgPreview.src = e.target.result;
      }
    }
  </script>

  <!-- MODAL -->
  <script type="text/javascript">
    $(function() {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      $('.swalDefaultSuccess').click(function() {
        Toast.fire({
          icon: 'success',
          title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.swalDefaultInfo').click(function() {
        Toast.fire({
          icon: 'info',
          title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.swalDefaultError').click(function() {
        Toast.fire({
          icon: 'error',
          title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.swalDefaultWarning').click(function() {
        Toast.fire({
          icon: 'warning',
          title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.swalDefaultQuestion').click(function() {
        Toast.fire({
          icon: 'question',
          title: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });

      $('.toastrDefaultSuccess').click(function() {
        toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultInfo').click(function() {
        toastr.info('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultError').click(function() {
        toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultWarning').click(function() {
        toastr.warning('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });

      $('.toastsDefaultDefault').click(function() {
        $(document).Toasts('create', {
          title: 'Toast Title',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultTopLeft').click(function() {
        $(document).Toasts('create', {
          title: 'Toast Title',
          position: 'topLeft',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultBottomRight').click(function() {
        $(document).Toasts('create', {
          title: 'Toast Title',
          position: 'bottomRight',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultBottomLeft').click(function() {
        $(document).Toasts('create', {
          title: 'Toast Title',
          position: 'bottomLeft',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultAutohide').click(function() {
        $(document).Toasts('create', {
          title: 'Toast Title',
          autohide: true,
          delay: 750,
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultNotFixed').click(function() {
        $(document).Toasts('create', {
          title: 'Toast Title',
          fixed: false,
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultFull').click(function() {
        $(document).Toasts('create', {
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.',
          title: 'Toast Title',
          subtitle: 'Subtitle',
          icon: 'fas fa-envelope fa-lg',
        })
      });
      $('.toastsDefaultFullImage').click(function() {
        $(document).Toasts('create', {
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.',
          title: 'Toast Title',
          subtitle: 'Subtitle',
          image: '<?= base_url(); ?>/dist/img/user3-128x128.jpg',
          imageAlt: 'User Picture',
        })
      });
      $('.toastsDefaultSuccess').click(function() {
        $(document).Toasts('create', {
          class: 'bg-success',
          title: 'Toast Title',
          subtitle: 'Subtitle',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultInfo').click(function() {
        $(document).Toasts('create', {
          class: 'bg-info',
          title: 'Toast Title',
          subtitle: 'Subtitle',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultWarning').click(function() {
        $(document).Toasts('create', {
          class: 'bg-warning',
          title: 'Toast Title',
          subtitle: 'Subtitle',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultDanger').click(function() {
        $(document).Toasts('create', {
          class: 'bg-danger',
          title: 'Toast Title',
          subtitle: 'Subtitle',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
      $('.toastsDefaultMaroon').click(function() {
        $(document).Toasts('create', {
          class: 'bg-maroon',
          title: 'Toast Title',
          subtitle: 'Subtitle',
          body: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.'
        })
      });
    });
  </script>

  <script>
    function previewImgLogo() {
      const logo = document.querySelector('#logo');
      const logoLabel = document.querySelector('.custom-file-label');
      const imgPreviewLogo = document.querySelector('.img-preview-logo');

      logoLabel.textContent = logo.files[0].name;

      const fileLogo = new FileReader();
      fileLogo.readAsDataURL(logo.files[0]);

      fileLogo.onload = function(e) {
        imgPreviewLogo.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgBanner() {
      const banner = document.querySelector('#banner');
      const bannerLabel = document.querySelector('.custom-file-label');
      const imgPreviewBanner = document.querySelector('.img-preview-banner');

      bannerLabel.textContent = banner.files[0].name;

      const fileBanner = new FileReader();
      fileBanner.readAsDataURL(banner.files[0]);

      fileBanner.onload = function(e) {
        imgPreviewBanner.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgFotokepsek() {
      const fotokepsek = document.querySelector('#fotokepsek');
      const fotokepsekLabel = document.querySelector('.custom-file-label');
      const imgPreviewKepsek = document.querySelector('.img-preview-fotokepsek');

      fotokepsekLabel.textContent = fotokepsek.files[0].name;

      const fileFotokepsek = new FileReader();
      fileFotokepsek.readAsDataURL(fotokepsek.files[0]);

      fileFotokepsek.onload = function(e) {
        imgPreviewKepsek.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImgModul() {
      const bg_modul = document.querySelector('#bg_modul');
      const bg_modulLabel = document.querySelector('.custom-file-label');
      const imgPreviewModul = document.querySelector('.img-preview-modul');

      bg_modulLabel.textContent = bg_modul.files[0].name;

      const fileBg_modul = new FileReader();
      fileBg_modul.readAsDataURL(bg_modul.files[0]);

      fileBg_modul.onload = function(e) {
        imgPreviewModul.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImg() {
      const slider = document.querySelector('#slider');
      const sliderLabel = document.querySelector('.custom-file-label');
      const imgPreviewSlider = document.querySelector('.img-preview');

      sliderLabel.textContent = slider.files[0].name;

      const fileSlider = new FileReader();
      fileSlider.readAsDataURL(slider.files[0]);

      fileSlider.onload = function(e) {
        imgPreviewSlider.src = e.target.result;
      }
    }
  </script>
  <script>
    function previewImg() {
      const sarana = document.querySelector('#sarana');
      const saranaLabel = document.querySelector('.custom-file-label');
      const imgPreviewSarana = document.querySelector('.img-preview');

      saranaLabel.textContent = sarana.files[0].name;

      const fileSarana = new FileReader();
      fileSarana.readAsDataURL(sarana.files[0]);

      fileSarana.onload = function(e) {
        imgPreviewSarana.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImg() {
      const image = document.querySelector('#image');
      const imageLabel = document.querySelector('.custom-file-label');
      const imgPreviewImage = document.querySelector('.img-preview');

      imageLabel.textContent = image.files[0].name;

      const fileImage = new FileReader();
      fileImage.readAsDataURL(image.files[0]);

      fileImage.onload = function(e) {
        imgPreviewImage.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImg() {
      const galeri = document.querySelector('#galeri');
      const galeriLabel = document.querySelector('.custom-file-label');
      const imgPreviewGaleri = document.querySelector('.img-preview');

      galeriLabel.textContent = galeri.files[0].name;

      const fileGaleri = new FileReader();
      fileGaleri.readAsDataURL(galeri.files[0]);

      fileGaleri.onload = function(e) {
        imgPreviewGaleri.src = e.target.result;
      }
    }
  </script>

  <script>
    function previewImg() {
      const berita = document.querySelector('#berita');
      const beritaLabel = document.querySelector('.custom-file-label');
      const imgPreviewBerita = document.querySelector('.img-preview');

      beritaLabel.textContent = berita.files[0].name;

      const fileBerita = new FileReader();
      fileBerita.readAsDataURL(berita.files[0]);

      fileBerita.onload = function(e) {
        imgPreviewBerita.src = e.target.result;
      }
    }
  </script>

  <script>
    $(document).ready(function() {
      $('#sejarah').summernote({
        tabsize: 2,
        height: 500
      });
    })
  </script>

  <script>
    $(document).ready(function() {
      $('#visimisi').summernote({
        tabsize: 2,
        height: 500
      });
    })
  </script>

  <?= $this->renderSection('extra-js') ?>

</body>

</html>