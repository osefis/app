<?= $this->extend('layout/backend/template'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('/sliders/create'); ?>" class="btn btn-sm btn-primary">Tambah Data Slider</a>
        </h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">

        <!-- Default box -->

        <!-- END HEADER -->

        <div class="container">
          <div class="row">
            <div class="col">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Posisi gambar</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  <?php foreach ($sliders as $s) : ?>
                    <tr>
                      <th scope="row"><?= $i++; ?></th>
                      <td><img src="/slider/<?= $s['image']; ?>" alt="" class="image" height="75"></td>
                      <td><?= $s['title']; ?></td>
                      <td><?= $s['status']; ?></td>
                      <td>
                        <a href="/sliders/<?= $s['slug']; ?>" class="btn btn-xs btn-success">Detail</a>
                        <a href="/sliders/edit/<?= $s['slug']; ?>" class="btn btn-xs btn-warning">Edit</a>
                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modal-danger">
                          Hapus
                        </button>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-danger">
                      <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                          <div class="modal-header">
                            <h4 class="modal-title">Hapus data berita</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <p>Apakah anda yakin akan gambar slider <?= $s['title']; ?></p>
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
                            <form action="/sliders/<?= $s['id']; ?>" method="post" class="d-inline">
                              <input type="hidden" name="_method" value="DELETE">
                              <?= csrf_field(); ?>
                              <button type="submit" class="btn btn-warning toastsDefaultSuccess">
                                Ya, Hapus data.
                              </button>
                            </form>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                  <?php endforeach; ?>
                </tbody>
              </table>

            </div>
          </div>
        </div>

        <?= $this->endSection(); ?>