<?= $this->extend('layout/backend/template'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('/sliders'); ?>" class="btn btn-sm btn-default">Kembali</a>
        </h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">

        <!-- Default box -->

        <!-- END HEADER -->

        <div class="container">
          <div class="row">
            <div class="col-8">

              <form action="/sliders/save" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <div class="form-group row">
                  <label for="title_dua" class="col-sm-2 col-form-label">Posisi gambar</label>
                  <div class="col-sm-10">
                    <select name="status" id="status" class="form-control">
                      <option value="item">Tambah Slider</option>
                      <option value="item active">Slider Awal</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="title" class="col-sm-2 col-form-label">Judul 1</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control <?= ($validation->hasError('title')) ? 'is-invalid' : ''; ?>" id="title" name="title" autofocus value="<?= old('title'); ?>">
                    <div class="invalid-feedback">
                      <?= $validation->getError('title'); ?>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="judul_link" class="col-sm-2 col-form-label">Judul Link 1</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="judul_link" name="judul_link" value="<?= old('judul_link'); ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="link_judul" class="col-sm-2 col-form-label">Link 1</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="link_judul" name="link_judul" value="<?= old('link_judul'); ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="judul_link_dua" class="col-sm-2 col-form-label">Judul Link 2</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="judul_link_dua" name="judul_link_dua" value="<?= old('judul_link_dua'); ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="link_judul_dua" class="col-sm-2 col-form-label">Link 2</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="link_judul_dua" name="link_judul_dua" value="<?= old('link_judul_dua'); ?>">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="body" class="col-sm-2 col-form-label">Deskripsi</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" id="body" name="body" value="<?= old('body'); ?>"></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="image" class="col-sm-2 col-form-label">Gambar</label>
                  <div class="col-sm-2">
                    <img src="<?= base_url('/slider/default.jpg'); ?>" class="img-thumbnail img-preview">
                  </div>
                  <div class="col-sm-8">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input <?= ($validation->hasError('image')) ? 'is-invalid' : ''; ?>" id="image" name="image" onchange="previewImg()">
                      <div class="invalid-feedback">
                        <?= $validation->getError('image'); ?>
                      </div>
                      <label class="custom-file-label" for="image">Pilih gambar..</label>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Tambah Data</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>

        <?= $this->endSection(); ?>