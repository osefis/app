<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Home | Corlate</title>

  <!-- core CSS -->
  <link href="<?= base_url(); ?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>/css/animate.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>/css/prettyPhoto.css" rel="stylesheet">
  <link href="<?= base_url(); ?>/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>/css/icomoon.css" rel="stylesheet">
  <link href="<?= base_url(); ?>/css/main.css" rel="stylesheet">
  <link href="<?= base_url(); ?>/css/responsive.css" rel="stylesheet">
  <!--[if lt IE 9]>
    <script src="<?= base_url(); ?>/js/html5shiv.js"></script>
    <script src="<?= base_url(); ?>/js/respond.min.js"></script>
    <![endif]-->
  <link rel="shortcut icon" href="<?= base_url(); ?>/images/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= base_url(); ?>/images/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= base_url(); ?>/images/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= base_url(); ?>/images/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?= base_url(); ?>/images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body class="homepage">

  <?= $this->include('templates/corlate/header'); ?>

  <?= $this->renderSection('content-home'); ?>


  <?= $this->include('modules/footer'); ?>

  <script src="<?= base_url(); ?>/js/jquery.js"></script>
  <script src="<?= base_url(); ?>/js/bootstrap.min.js"></script>
  <script src="<?= base_url(); ?>/js/jquery.prettyPhoto.js"></script>
  <script src="<?= base_url(); ?>/js/owl.carousel.min.js"></script>
  <script src="<?= base_url(); ?>/js/jquery.isotope.min.js"></script>
  <script src="<?= base_url(); ?>/js/main.js"></script>
</body>

</html>