<?= $this->extend('layout/backend/template'); ?>

<?= $this->section('content'); ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('/galeries'); ?>" class="btn btn-sm btn-default">Kembali</a>
        </h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">

        <!-- Default box -->

        <!-- END HEADER -->

        <div class="container">
          <div class="row">
            <div class="col-8">

              <h2 class="my-3">Form Ubah Data Galeri</h2>

              <form action="/galeries/update/<?= $galeries['id']; ?>" method="post" enctype="multipart/form-data">
                <?= csrf_field(); ?>
                <input type="hidden" name="slug" value="<?= $galeries['slug']; ?>">
                <input type="hidden" name="imageLama" value="<?= $galeries['image']; ?>">
                <div class="form-group row">
                  <label for="title" class="col-sm-2 col-form-label">Judul</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control <?= ($validation->hasError('title')) ? 'is-invalid' : ''; ?>" id="title" name="title" autofocus value="<?= (old('title')) ? old('title') : $galeries['title']; ?>">
                    <div class="invalid-feedback">
                      <?= $validation->getError('title'); ?>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="body" class="col-sm-2 col-form-label">Penerbit</label>
                  <div class="col-sm-10">
                    <select name="category" id="category" class="form-control">
                      <option value="kegiatan" <?php if ($galeries == 'kegiatan') echo "selected"; ?>>Kegiatan</option>
                      <option value="sarana" <?php if ($galeries == 'sarana') echo "selected"; ?>>Sarana Sekolah</option>
                      <option value="ekstrakurikuler" <?php if ($galeries == 'ekstrakurikuler') echo "selected"; ?>>Ekstrakurikuler</option>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="image" class="col-sm-2 col-form-label">Sampul</label>
                  <div class="col-sm-2">
                    <img src="/galeri/<?= $galeries['image']; ?>" class="img-thumbnail img-preview">
                  </div>
                  <div class="col-sm-8">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input <?= ($validation->hasError('image')) ? 'is-invalid' : ''; ?>" id="image" name="image" onchange="previewImg()">
                      <div class="invalid-feedback">
                        <?= $validation->getError('image'); ?>
                      </div>
                      <label class="custom-file-label" for="image"><?= $galeries['image']; ?></label>
                    </div>
                  </div>
                </div>


                <div class="form-group row">
                  <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Ubah Data</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>



        <?= $this->endSection(); ?>

        <?= $this->section('extra-js') ?>
        <!-- include summernote css/js -->
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-bs4.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote-bs4.min.js"></script>
        <script>
          $(document).ready(function() {
            $('#body').summernote({
              tabsize: 2,
              height: 500
            });
          })
        </script>
        <?= $this->endSection(); ?>