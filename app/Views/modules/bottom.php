<section id="bottom">
  <div class="container fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
    <div class="row">
      <div class="col-md-2">
        <a href="<?= base_url(); ?>/#" class="footer-logo">
          <img src="<?= base_url(); ?>/images/logo-black.png" alt="logo">
        </a>
      </div>
      <div class="col-md-10">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <div class="widget">
              <h3>Company</h3>
              <ul>
                <li><a href="<?= base_url(); ?>/#">About us</a></li>
                <li><a href="<?= base_url(); ?>/#">We are hiring</a></li>
                <li><a href="<?= base_url(); ?>/#">Meet the team</a></li>
                <li><a href="<?= base_url(); ?>/#">Copyright</a></li>
                <li><a href="<?= base_url(); ?>/#">Terms of use</a></li>
                <li><a href="<?= base_url(); ?>/#">Privacy policy</a></li>
                <li><a href="<?= base_url(); ?>/#">Contact us</a></li>
              </ul>
            </div>
          </div>
          <!--/.col-md-3-->

          <div class="col-md-3 col-sm-6">
            <div class="widget">
              <h3>Support</h3>
              <ul>
                <li><a href="<?= base_url(); ?>/#">Faq</a></li>
                <li><a href="<?= base_url(); ?>/#">Blog</a></li>
                <li><a href="<?= base_url(); ?>/#">Forum</a></li>
                <li><a href="<?= base_url(); ?>/#">Documentation</a></li>
                <li><a href="<?= base_url(); ?>/#">Refund policy</a></li>
                <li><a href="<?= base_url(); ?>/#">Ticket system</a></li>
                <li><a href="<?= base_url(); ?>/#">Billing system</a></li>
              </ul>
            </div>
          </div>
          <!--/.col-md-3-->

          <div class="col-md-3 col-sm-6">
            <div class="widget">
              <h3>Developers</h3>
              <ul>
                <li><a href="<?= base_url(); ?>/#">Web Development</a></li>
                <li><a href="<?= base_url(); ?>/#">SEO Marketing</a></li>
                <li><a href="<?= base_url(); ?>/#">Theme</a></li>
                <li><a href="<?= base_url(); ?>/#">Development</a></li>
                <li><a href="<?= base_url(); ?>/#">Email Marketing</a></li>
                <li><a href="<?= base_url(); ?>/#">Plugin Development</a></li>
                <li><a href="<?= base_url(); ?>/#">Article Writing</a></li>
              </ul>
            </div>
          </div>
          <!--/.col-md-3-->

          <div class="col-md-3 col-sm-6">
            <div class="widget">
              <h3>Our Partners</h3>
              <ul>
                <li><a href="<?= base_url(); ?>/#">Adipisicing Elit</a></li>
                <li><a href="<?= base_url(); ?>/#">Eiusmod</a></li>
                <li><a href="<?= base_url(); ?>/#">Tempor</a></li>
                <li><a href="<?= base_url(); ?>/#">Veniam</a></li>
                <li><a href="<?= base_url(); ?>/#">Exercitation</a></li>
                <li><a href="<?= base_url(); ?>/#">Ullamco</a></li>
                <li><a href="<?= base_url(); ?>/#">Laboris</a></li>
              </ul>
            </div>
          </div>
          <!--/.col-md-3-->
        </div>
      </div>


    </div>
  </div>
</section>
<!--/#bottom-->