<footer id="footer" class="midnight-blue">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        &copy; <?= date('Y'); ?> <a target="_blank" href="<?= base_url(); ?>/http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
      </div>
      <div class="col-sm-6">
        <ul class="pull-right">
          <li><a href="<?= base_url(); ?>/#">Home</a></li>
          <li><a href="<?= base_url(); ?>/#">About Us</a></li>
          <li><a href="<?= base_url(); ?>/#">Faq</a></li>
          <li><a href="<?= base_url(); ?>/#">Contact Us</a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
<!--/#footer-->