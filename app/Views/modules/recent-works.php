<section id="recent-works">
  <div class="container">
    <div class="center fadeInDown">
      <h2>Foto Terbaru</h2>
      <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
    </div>

    <div class="row">
      <?php foreach ($galeries as $g) : ?>
        <div class="col-xs-12 col-sm-6 col-md-4 single-work">
          <div class="recent-work-wrap">
            <img class="img-responsive" src="<?= base_url(); ?>/galeri/<?= $g['image']; ?>" alt="">
            <div class="overlay">
              <div class="recent-work-inner">
                <a class="preview" href="<?= base_url(); ?>/galeri/<?= $g['image']; ?>" rel="prettyPhoto"><i class="fa fa-plus"></i></a>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach; ?>


    </div>
    <!--/.row-->
    <div class="clearfix text-center">
      <br>
      <br>
      <a href="<?= base_url(); ?>/album" class="btn btn-primary">Lihat Galeri</a>
    </div>
  </div>
  <!--/.container-->
</section>
<!--/#recent-works-->