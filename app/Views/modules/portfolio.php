<div class="page-title" style="background-image: url(images/page-title.png)">
  <h1>Portfolio</h1>
</div>

<section id="portfolio">
  <div class="container">
    <div class="center">
      <h2>Galeri Foto</h2>
      <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
    </div>


    <ul class="portfolio-filter text-center">
      <li><a class="btn btn-default active" href="#" data-filter="*">Semua Foto</a></li>
      <li><a class="btn btn-default" href="#" data-filter=".kegiatan">Kegiatan</a></li>
      <li><a class="btn btn-default" href="#" data-filter=".sarana">Sarana</a></li>
      <li><a class="btn btn-default" href="#" data-filter=".ekstrakurikuler">Ekstrakurikuler</a></li>
    </ul>
    <!--/#portfolio-filter-->

    <div class="row">
      <div class="portfolio-items">
        <?php foreach ($galeries as $g) : ?>
          <div class="portfolio-item <?= $g['category']; ?> col-xs-12 col-sm-4 col-md-3 single-work">
            <div class="recent-work-wrap">
              <img class="img-responsive" src="<?= base_url(); ?>/galeri/<?= $g['image']; ?>" alt="">
              <div class="overlay">
                <div class="recent-work-inner">
                  <a class="preview" href="<?= base_url(); ?>/galeri/<?= $g['image']; ?>" rel="prettyPhoto"><i class="fa fa-plus"></i></a>
                </div>
              </div>
            </div>
          </div>
          <!--/.portfolio-item-->

        <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>
<!--/#portfolio-item-->