<section id="main-slider" class="no-margin">
  <div class="carousel slide">
    <ol class="carousel-indicators">
      <li data-target="#main-slider" data-slide-to="0" class="active"></li>
      <li data-target="#main-slider" data-slide-to="1"></li>
      <li data-target="#main-slider" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <?php foreach ($sliders as $s) : ?>
        <div class="<?= $s['status']; ?>" style="background-image: url(slider/<?= $s['image']; ?>)">
          <div class="container">
            <div class="row">
              <div class="col-md-7">
                <div class="carousel-content">
                  <h1 class="animation animated-item-1"><?= $s['title']; ?></h1>
                  <div class="animation animated-item-2">
                    <?= $s['body']; ?>
                  </div>
                  <a class="btn-slide animation animated-item-3" href="<?= $s['link_judul']; ?>" target="_blank"><?= $s['judul_link']; ?></a>
                  <a class="btn-slide white animation animated-item-3" href="<?= $s['link_judul_dua']; ?>" target="_blank"><?= $s['judul_link_dua']; ?></a>
                </div>
              </div>

            </div>
          </div>
        </div>
      <?php endforeach; ?>
      <!--/.item-->

    </div>
    <!--/.carousel-inner-->
  </div>
  <!--/.carousel-->
  <a class="prev hidden-xs hidden-sm" href="<?= base_url(); ?>/#main-slider" data-slide="prev">
    <i class="fa fa-chevron-left"></i>
  </a>
  <a class="next hidden-xs hidden-sm" href="<?= base_url(); ?>/#main-slider" data-slide="next">
    <i class="fa fa-chevron-right"></i>
  </a>
</section>
<!--/#main-slider-->