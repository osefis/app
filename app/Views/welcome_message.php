<?= $this->extend('templates/corlate/template'); ?>

<?= $this->section('content-home'); ?>

  <?= $this->include('modules/slider'); ?>
  <?= $this->include('modules/content'); ?>
  <?= $this->include('modules/partner'); ?>
  <?= $this->include('modules/feature'); ?>
  <?= $this->include('modules/services'); ?>
  <?= $this->include('modules/middle'); ?>
  <?= $this->include('modules/testimonial'); ?>
  <?= $this->include('modules/recent-works'); ?>
  <?= $this->include('modules/bottom'); ?>

<?= $this->endSection(); ?>