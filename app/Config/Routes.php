<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

// NEWS-BERITA
$routes->get('/administrator/news', 'News::index');
$routes->get('/administrator/news/create', 'News::create');
$routes->get('/administrator/news/edit/(:segment)', 'News::edit/$1');
$routes->delete('/administrator/news/(:num)', 'News::delete/$1');
$routes->get('/administrator/news/(:any)', 'News::detail/$1');
$routes->get('/berita/detail_berita/(:any)', 'Berita::detail/$1');

// GALERI
$routes->get('/administrator/galeries', 'Galeries::index');
$routes->get('/administrator/galeries/create', 'Galeries::create');
$routes->get('/administrator/galeries/edit/(:segment)', 'Galeries::edit/$1');
$routes->delete('/administrator/galeries/(:num)', 'Galeries::delete/$1');
$routes->get('/administrator/galeries/(:any)', 'Galeries::detail/$1');
$routes->get('/album/(:any)', 'Album::detail/$1');

// SLIDER
$routes->get('/administrator/sliders', 'Sliders::index');
$routes->get('/administrator/sliders/create', 'Sliders::create');
$routes->get('/administrator/sliders/edit/(:segment)', 'Sliders::edit/$1');
$routes->delete('/administrator/sliders/(:num)', 'Sliders::delete/$1');
$routes->get('/administrator/sliders/(:any)', 'Sliders::detail/$1');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
