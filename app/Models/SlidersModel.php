<?php

namespace App\Models;

use CodeIgniter\Model;

class SlidersModel extends Model
{
  protected $table      = 'sliders';
  protected $useTimestamps = true;
  protected $allowedFields = ['title', 'slug', 'body', 'judul_link', 'link_judul', 'judul_link_dua', 'link_judul_dua', 'status', 'image'];

  public function getSliders($slug = false)
  {
    if ($slug == false) {
      return $this->findAll();
    }

    return $this->where(['slug' => $slug])->first();
  }
}
