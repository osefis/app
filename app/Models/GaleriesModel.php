<?php

namespace App\Models;

use CodeIgniter\Model;

class GaleriesModel extends Model
{
  protected $table      = 'galeries';
  protected $useTimestamps = true;
  protected $allowedFields = ['title', 'slug', 'category', 'image'];

  public function getGaleries($slug = false)
  {
    if ($slug == false) {
      return $this->findAll();
    }

    return $this->where(['slug' => $slug])->first();
  }
}
