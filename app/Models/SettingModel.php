<?php

namespace App\Models;

use CodeIgniter\Model;

class SettingModel extends Model
{
  protected $table      = 'setting';
  protected $useTimestamps = true;
  protected $allowedFields = ['npsn', 'sekolah', 'slug', 'kepsek', 'fotokepsek', 'alamat', 'telp', 'facebook', 'twitter', 'instagram', 'logo', 'banner', 'pengantar', 'sejarah', 'visimisi', 'lokasi', 'video'];

  public function getSetting($slug = false)
  {
    if ($slug == false) {
      return $this->findAll();
    }

    return $this->where(['slug' => $slug])->first();
  }
}
