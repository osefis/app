<?php

namespace App\Controllers;

use App\Models\NewsModel;

class News extends BaseController
{
  protected $newsModel;

  public function __construct()
  {
    $this->newsModel = new NewsModel();
  }

  public function index()
  {
    $data = [
      'title' => 'News | Ayosinau',
      'news' => $this->newsModel->getNews(),
      'lihat' => $this->settingModel->getSetting(),
    ];

    return view('news/index', $data);
  }

  public function detail($slug)
  {

    $data = [
      'title' => 'Detail News | Ayosinau',
      'news' => $this->newsModel->getNews($slug)
    ];

    // Jika news tidak ada di tabel
    if (empty($data['news'])) {
      throw new \CodeIgniter\Exceptions\PageNotFoundException('Judul News ' . $slug . ' tidak ditemukan');
    }

    return view('news/detail', $data);
  }

  public function create()
  {
    // session(); ditaruh di basecontroller
    $data = [
      'title' => 'Form Tambah Data News | Ayosinau',
      'validation' => \Config\Services::validation()
    ];

    return view('news/create', $data);
  }

  public function save()
  {
    // Validasi input
    if (!$this->validate([
      'title' => [
        'rules' => 'required|is_unique[news.title]',
        'errors' => [
          'required' => '{field} news harus diisi.',
          'is_unique' => '{field} news sudah terdaftar'
        ]
      ],
      'image' => [
        'rules' => 'max_size[image,1024]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {

      return redirect()->to('/news/create')->withInput();
    }

    // Ambil gambar
    $fileImage = $this->request->getFile('image');
    // Apakah tidak ada gambar yang diupload
    if ($fileImage->getError() == 4) {
      $namaImage = 'default.jpg';
    } else {
      // generate nama image random
      $namaImage = $fileImage->getRandomName();
      // Pindahlan file ke folder img
      $fileImage->move('img', $namaImage);
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->newsModel->save([
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'body' => $this->request->getVar('body'),
      'created_at' => time(),
      'updated_at' => time(),
      'image' => $namaImage
    ]);

    session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

    return redirect()->to('/news');
  }

  public function delete($id)
  {
    // Cari gambar bedasarkan id
    $news = $this->newsModel->find($id);

    // cek jika gambarnya default.jpg
    if ($news['image'] != 'default.jpg') {
      // Hapus gambar
      unlink('img/' . $news['image']);
    }

    $this->newsModel->delete($id);
    // session()->setFlashdata('pesan', 'Data berhasil dihapus.');

    return redirect()->to('/news');
  }

  public function edit($slug)
  {
    $data = [
      'title' => 'Form Ubah Data News | Ayosinau',
      'validation' => \Config\Services::validation(),
      'news' => $this->newsModel->getNews($slug)
    ];

    return view('news/edit', $data);
  }

  public function update($id)
  {
    //  Cek Judul
    $newsLama = $this->newsModel->getNews($this->request->getVar('slug'));
    if ($newsLama['title'] == $this->request->getVar('title')) {
      $rule_title = 'required';
    } else {
      $rule_title = 'required|is_unique[news.title]';
    }
    if (!$this->validate([
      'title' => [
        'rules' => $rule_title,
        'errors' => [
          'required' => '{field} news harus diisi.',
          'is_unique' => '{field} news sudah terdaftar'
        ]
      ],
      'image' => [
        'rules' => 'max_size[image,1024]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {
      return redirect()->to('/news/edit/' . $this->request->getVar('slug'))->withInput();
    }

    $fileImage = $this->request->getFile('image');

    // Cek gambar, apakah tetap gambar lama
    if ($fileImage->getError() == 4) {
      $namaImage = $this->request->getVar('imageLama');
    } else {
      // generate nama file random
      $namaImage = $fileImage->getRandomName();
      // pindahkan gambar
      $fileImage->move('img', $namaImage);
      // Hapus file yang lama
      unlink('img/' . $this->request->getVar('imageLama'));
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->newsModel->save([
      'id' => $id,
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'body' => $this->request->getVar('body'),
      'updated_at' => time(),
      'image' => $namaImage
    ]);

    session()->setFlashdata('pesan', 'Data berhasil diubah.');

    return redirect()->to('/news');
  }

  //--------------------------------------------------------------------

}
