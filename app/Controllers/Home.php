<?php

namespace App\Controllers;

use App\Models\SlidersModel;
use App\Models\GaleriesModel;
// use App\Models\SettingModel;

class Home extends BaseController
{
	protected $slidersModel;
	protected $galeriesModel;
	// protected $settingModel;

	public function __construct()
	{
		$this->slidersModel = new SlidersModel();
		$this->galeriesModel = new GaleriesModel();
		// $this->settingModel = new SettingModel();
	}

	public function index()
	{
		$data = [
			'title' => 'News | Ayosinau',
			'sliders' => $this->slidersModel->getSliders(),
			'galeries' => $this->galeriesModel->getGaleries(),
			'setting' => $this->settingModel->getSetting(),
			// 'lihat' => $this->settingModel->getSetting()
		];
		return view('welcome_message', $data);
	}
}
