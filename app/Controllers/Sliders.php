<?php

namespace App\Controllers;

use App\Models\SlidersModel;

class Sliders extends BaseController
{
  protected $slidersModel;

  public function __construct()
  {
    $this->slidersModel = new SlidersModel();
  }

  public function index()
  {
    // $sliders = $this->slidersModel->findAll();
    $data = [
      'title' => 'Sliders | Ayosinau',
      'sliders' => $this->slidersModel->getSliders(),
      'lihat' => $this->settingModel->getSetting(),
    ];

    return view('sliders/index', $data);
  }

  public function detail($slug)
  {

    $data = [
      'title' => 'Detail Sliders | Ayosinau',
      'sliders' => $this->slidersModel->getSliders($slug)
    ];

    // Jika sliders tidak ada di tabel
    if (empty($data['sliders'])) {
      throw new \CodeIgniter\Exceptions\PageNotFoundException('Judul Sliders ' . $slug . ' tidak ditemukan');
    }

    return view('sliders/detail', $data);
  }

  public function create()
  {
    // session(); ditaruh di basecontroller
    $data = [
      'title' => 'Form Tambah Data Sliders | Ayosinau',
      'validation' => \Config\Services::validation()
    ];

    return view('sliders/create', $data);
  }

  public function save()
  {
    // Validasi input
    if (!$this->validate([
      'title' => [
        'rules' => 'required|is_unique[sliders.title]',
        'errors' => [
          'required' => '{field} sliders harus diisi.',
          'is_unique' => '{field} sliders sudah terdaftar'
        ]
      ],
      'image' => [
        'rules' => 'max_size[image,1024]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {

      return redirect()->to('/sliders/create')->withInput();
    }

    // Ambil gambar
    $fileImage = $this->request->getFile('image');
    // Apakah tidak ada gambar yang diupload
    if ($fileImage->getError() == 4) {
      $namaImage = 'default.jpg';
    } else {
      // generate nama image random
      $namaImage = $fileImage->getRandomName();
      // Pindahlan file ke folder slider
      $fileImage->move('slider', $namaImage);
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->slidersModel->save([
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'body' => $this->request->getVar('body'),
      'judul_link' => $this->request->getVar('judul_link'),
      'link_judul' => $this->request->getVar('link_judul'),
      'judul_link_dua' => $this->request->getVar('judul_link_dua'),
      'link_judul_dua' => $this->request->getVar('link_judul_dua'),
      'status' => $this->request->getVar('status'),
      'created_at' => time(),
      'updated_at' => time(),
      'image' => $namaImage
    ]);

    session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

    return redirect()->to('/sliders');
  }

  public function delete($id)
  {
    // Cari gambar bedasarkan id
    $sliders = $this->slidersModel->find($id);

    // cek jika gambarnya default.jpg
    if ($sliders['image'] != 'default.jpg') {
      // Hapus gambar
      unlink('slider/' . $sliders['image']);
    }

    $this->slidersModel->delete($id);
    // session()->setFlashdata('pesan', 'Data berhasil dihapus.');

    return redirect()->to('/sliders');
  }

  public function edit($slug)
  {
    $data = [
      'title' => 'Form Ubah Data Sliders | Ayosinau',
      'validation' => \Config\Services::validation(),
      'sliders' => $this->slidersModel->getSliders($slug),
      'lihat' => $this->settingModel->getSetting(),
    ];

    return view('sliders/edit', $data);
  }

  public function update($id)
  {
    //  Cek Judul
    $slidersLama = $this->slidersModel->getSliders($this->request->getVar('slug'));
    if ($slidersLama['title'] == $this->request->getVar('title')) {
      $rule_title = 'required';
    } else {
      $rule_title = 'required|is_unique[sliders.title]';
    }
    if (!$this->validate([
      'title' => [
        'rules' => $rule_title,
        'errors' => [
          'required' => '{field} sliders harus diisi.',
          'is_unique' => '{field} sliders sudah terdaftar'
        ]
      ],
      'image' => [
        'rules' => 'max_size[image,1024]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {
      return redirect()->to('/sliders/edit/' . $this->request->getVar('slug'))->withInput();
    }

    $fileImage = $this->request->getFile('image');

    // Cek gambar, apakah tetap gambar lama
    if ($fileImage->getError() == 4) {
      $namaImage = $this->request->getVar('imageLama');
    } else {
      // generate nama file random
      $namaImage = $fileImage->getRandomName();
      // pindahkan gambar
      $fileImage->move('slider', $namaImage);
      // Hapus file yang lama
      unlink('slider/' . $this->request->getVar('imageLama'));
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->slidersModel->save([
      'id' => $id,
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'body' => $this->request->getVar('body'),
      'judul_link' => $this->request->getVar('judul_link'),
      'link_judul' => $this->request->getVar('link_judul'),
      'judul_link_dua' => $this->request->getVar('judul_link_dua'),
      'link_judul_dua' => $this->request->getVar('link_judul_dua'),
      'status' => $this->request->getVar('status'),
      'updated_at' => time(),
      'image' => $namaImage
    ]);

    session()->setFlashdata('pesan', 'Data berhasil diubah.');

    return redirect()->to('/sliders');
  }

  //--------------------------------------------------------------------

}
