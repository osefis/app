<?php

namespace App\Controllers;

use App\Models\NewsModel;

class Berita extends BaseController
{
  protected $newsModel;

  public function __construct()
  {
    $this->newsModel = new NewsModel();
  }

  public function index()
  {
    // $news = $this->newsModel->findAll();
    $data = [
      'title' => 'News | Ayosinau',
      'news' => $this->newsModel->getNews(),
      'setting' => $this->settingModel->getSetting(),
    ];

    return view('berita', $data);
  }

  public function detail($slug)
  {
    $data = [
      'title' => 'Detail News | Ayosinau',
      'news' => $this->newsModel->getNews($slug),
      'setting' => $this->settingModel->getSetting(),
    ];

    // Jika news tidak ada di tabel
    if (empty($data['news'])) {
      throw new \CodeIgniter\Exceptions\PageNotFoundException('Judul News ' . $slug . ' tidak ditemukan');
    }

    return view('detail_berita', $data);
  }

  //--------------------------------------------------------------------

}
