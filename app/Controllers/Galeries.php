<?php

namespace App\Controllers;

use App\Models\GaleriesModel;

class Galeries extends BaseController
{
  protected $galeriesModel;

  public function __construct()
  {
    $this->galeriesModel = new GaleriesModel();
  }

  public function index()
  {
    $data = [
      'title' => 'Galeries | Ayosinau',
      'galeries' => $this->galeriesModel->getGaleries(),
      'lihat' => $this->settingModel->getSetting(),
      'setting' => $this->settingModel->getSetting(),
    ];

    return view('galeries/index', $data);
  }

  public function create()
  {
    $data = [
      'title' => 'Form Tambah Data Galeries | Ayosinau',
      'validation' => \Config\Services::validation()
    ];

    return view('galeries/create', $data);
  }

  public function save()
  {
    // Validasi input
    if (!$this->validate([
      'title' => [
        'rules' => 'required|is_unique[galeries.title]',
        'errors' => [
          'required' => '{field} galeries harus diisi.',
          'is_unique' => '{field} galeries sudah terdaftar'
        ]
      ],
      'image' => [
        'rules' => 'max_size[image,1024]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {

      return redirect()->to('/galeries/create')->withInput();
    }

    // Ambil gambar
    $fileImage = $this->request->getFile('image');
    // Apakah tidak ada gambar yang diupload
    if ($fileImage->getError() == 4) {
      $namaImage = 'default.jpg';
    } else {
      // generate nama image random
      $namaImage = $fileImage->getRandomName();
      // Pindahlan file ke folder galeri
      $fileImage->move('galeri', $namaImage);
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->galeriesModel->save([
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'category' => $this->request->getVar('category'),
      'created_at' => time(),
      'updated_at' => time(),
      'image' => $namaImage
    ]);

    session()->setFlashdata('pesan', 'Data berhasil ditambahkan.');

    return redirect()->to('/galeries');
  }

  public function delete($id)
  {
    // Cari gambar bedasarkan id
    $galeries = $this->galeriesModel->find($id);

    // cek jika gambarnya default.jpg
    if ($galeries['image'] != 'default.jpg') {
      // Hapus gambar
      unlink('galeri/' . $galeries['image']);
    }

    $this->galeriesModel->delete($id);
    // session()->setFlashdata('pesan', 'Data berhasil dihapus.');

    return redirect()->to('/galeries');
  }

  public function edit($slug)
  {
    $data = [
      'title' => 'Form Ubah Data Galeries | Ayosinau',
      'validation' => \Config\Services::validation(),
      'galeries' => $this->galeriesModel->getGaleries($slug)
    ];

    return view('galeries/edit', $data);
  }

  public function update($id)
  {
    //  Cek Judul
    $galeriesLama = $this->galeriesModel->getGaleries($this->request->getVar('slug'));
    if ($galeriesLama['title'] == $this->request->getVar('title')) {
      $rule_title = 'required';
    } else {
      $rule_title = 'required|is_unique[galeries.title]';
    }
    if (!$this->validate([
      'title' => [
        'rules' => $rule_title,
        'errors' => [
          'required' => '{field} galeries harus diisi.',
          'is_unique' => '{field} galeries sudah terdaftar'
        ]
      ],
      'image' => [
        'rules' => 'max_size[image,1024]|is_image[image]|mime_in[image,image/jpg,image/jpeg,image/png]',
        'errors' => [
          'max_size' => 'Ukuran gambar terlalu besar',
          'is_image' => 'Yang anda pilih bukan gambar',
          'mime_in' => 'Yang anda pilih bukan gambar'
        ]
      ]
    ])) {
      return redirect()->to('/galeries/edit/' . $this->request->getVar('slug'))->withInput();
    }

    $fileImage = $this->request->getFile('image');

    // Cek gambar, apakah tetap gambar lama
    if ($fileImage->getError() == 4) {
      $namaImage = $this->request->getVar('imageLama');
    } else {
      // generate nama file random
      $namaImage = $fileImage->getRandomName();
      // pindahkan gambar
      $fileImage->move('galeri', $namaImage);
      // Hapus file yang lama
      unlink('galeri/' . $this->request->getVar('imageLama'));
    }

    $slug = url_title($this->request->getVar('title'), '-', true);
    $this->galeriesModel->save([
      'id' => $id,
      'title' => $this->request->getVar('title'),
      'slug' => $slug,
      'category' => $this->request->getVar('category'),
      'updated_at' => time(),
      'image' => $namaImage
    ]);

    session()->setFlashdata('pesan', 'Data berhasil diubah.');

    return redirect()->to('/galeries');
  }

  //--------------------------------------------------------------------

}
