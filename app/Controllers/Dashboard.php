<?php

namespace App\Controllers;

use App\Models\Dashboard_model;

class Dashboard extends BaseController
{
  protected $settingModel;

  public function __construct()
  {
    $this->cek_login();
    $this->dashboard_model = new Dashboard_model();
  }

  public function index()
  {
    $data = [
      'title' => 'News | Ayosinau',
      'lihat' => $this->settingModel->getSetting(),
    ];
    if ($this->cek_login() == FALSE) {
      session()->setFlashdata('error_login', 'Silahkan login terlebih dahulu untuk mengakses data');
      return redirect()->to('/administrator');
    }

    return view('admin/dashboard', $data);
  }
}
